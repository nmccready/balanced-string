// Write a function that returns true if the parentheses
// in a given string are balanced. (ignoring any other characters)
// The function must handle
// parens (), square brackets [], and curly braces {}.

// '' -> true
// '[a]' -> true
// 'f(e(d))' -> true
// '[()]{}([])' -> true
// '(a[0]+b[2c[6]]) {24 + 53}' -> true

// '(c]' -> false
// '((b)' -> false
// '([)]' -> false
// '{(a[])' -> false


//((parensLeft.charAt(0) === '}' &&  !parensSeen.length) || parensLeft.charAt(parensLeft.length - 1) === '}')

function isBalanced(str) {
  if (!str) // covers empty string, undefined, null
    return true;


  var cleanStrOfOnlyParens = str.replace(/\w*/g, '');

  function loop(parensLeft, parensSeen, isBal) {
      if (!parensLeft.length)
        return isBal;
      // head is either close paren or bracket,
      // invalid start
      else if ((parensLeft.charAt(0) === ')' && !parensSeen.length) || parensLeft.charAt(parensLeft.length - 1) ===
        '(') {
        return loop([], [], false)
      } else if (!parensSeen.length) {
        // loop(tail, append to parensSeen)
        return loop(parensLeft.slice(1), parensSeen.concat(parensLeft.charAt(0)))
      } else { // continue on
        var isEven = (parensLeft.length + parensSeen.length) % 2 == 0;
        console.log("parensLeft.length: ", parensLeft.length);
        console.log("parensSeen.length: ", parensSeen.length);
        console.log("isEven: ", isEven);
        return loop(parensLeft.slice(1), parensSeen.concat(parensLeft.charAt(0)), isEven);
      }
    }

    return loop(cleanStrOfOnlyParens, [], true);
  }

  console.log(isBalanced('f(e(d))'));

